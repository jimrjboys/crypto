import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DescryptPage } from './descrypt.page';

describe('DescryptPage', () => {
  let component: DescryptPage;
  let fixture: ComponentFixture<DescryptPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DescryptPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DescryptPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
