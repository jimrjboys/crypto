import { Component, OnInit } from '@angular/core';
import * as CryptoJS from 'crypto-js'
import {Router} from '@angular/router';
@Component({
  selector: 'app-encrypt',
  templateUrl: './encrypt.page.html',
  styleUrls: ['./encrypt.page.scss'],
})
export class EncryptPage implements OnInit {

  encryptedData : String ;
  secretKey : string ;
  messages: any;
  constructor(    private  router :  Router ) { }

  ngOnInit() {
  }
  crypte(form){
    this.messages = form.value.msg ;
    this.secretKey = form.value.key ;
    this.encryptedData = CryptoJS.AES.encrypt(JSON.stringify(this.messages),this.secretKey).toString() ;
}
goBack(){

  //retour en arriere
  this.router.navigate(['home']);
}


}
