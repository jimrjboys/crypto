import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DescryptPage } from './descrypt.page';

const routes: Routes = [
  {
    path: '',
    component: DescryptPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DescryptPageRoutingModule {}
