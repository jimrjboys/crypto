import { Component, OnInit } from '@angular/core';
import * as CryptoJS from 'crypto-js'
import {Router} from '@angular/router';
@Component({
  selector: 'app-descrypt',
  templateUrl: './descrypt.page.html',
  styleUrls: ['./descrypt.page.scss'],
})
export class DescryptPage implements OnInit {
  messages: any;
  secretKey: any;
  code: any;

  constructor(    private  router :  Router ) { }

  ngOnInit() {
  }
  crypte(form){
    this.messages = form.value.msg ;
    this.secretKey = form.value.key ;
    let bytes = CryptoJS.AES.decrypt(this.messages,this.secretKey);
    this.code = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
    console.log (this.code)
}
goBack(){

  //retour en arriere
  this.router.navigate(['home']);
}

}
