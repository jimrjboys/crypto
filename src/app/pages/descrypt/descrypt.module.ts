import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DescryptPageRoutingModule } from './descrypt-routing.module';

import { DescryptPage } from './descrypt.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DescryptPageRoutingModule
  ],
  declarations: [DescryptPage]
})
export class DescryptPageModule {}
