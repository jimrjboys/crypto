import { Component } from '@angular/core';
import { Router } from '@angular/router';

import * as CryptoJS from 'crypto-js'
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  // dataTEncrypt:any = {id:'abc123',name:'demoname'} ;
  // dataTEncrypt:any = "Ty tena fomby " ;
  encryptedData : String ;
  secretKey : string ;
  url: any;
  messages: any;


  constructor(private router :  Router) {
  }
  versEncrypt(){

    this.router.navigate(['encrypt']) ; 

  }
  versDescrypt(){

    this.router.navigate(['descrypt']) ; 

  }
  Encrypt(){
  //  this.encryptedData = CryptoJS.AES.encrypt(JSON.stringify(this.dataTEncrypt),this.secretKey).toString() ;
  //  console.log(this.encryptedData);
  }
  Decrypt(){
      let bytes = CryptoJS.AES.decrypt(this.encryptedData,this.secretKey);
      var obj = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
      console.log (obj)
      // alert("id = "+ obj.id);
      // alert("name = " +obj.name);

  }
  crypte(form){
      
      this.messages = form.value.msg ;
      this.secretKey = form.value.key ;
      this.encryptedData = CryptoJS.AES.encrypt(JSON.stringify(this.messages),this.secretKey).toString() ;
      
   
  }
}
